<h1>Game Jam (Sujet : Inversion) - Raclette</h1>

<p><h2>Description du travail</h2>
Puzzle Game en 2D où le but est d'atteindre la sortie d'un niveau en 
activant/désactivant des portes à l'aide de boutons et de statues.
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>Clion
    <li>SDL2
    <li>C++
    <li>Git
</ul></p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Game Designer
    <li>Programmeur
</ul></p>

<p><h2>Participants</h2>
<ul><li>Stiven Aigle
    <li>Ludovic Jozereau
    <li>Victor Mottet
</ul></p>

<p><h3>Pas d'exécutable disponible actuellement.</h3></p>
