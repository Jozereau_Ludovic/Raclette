#pragma once
#include "../Classe/Niveau.hpp"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include <iostream>
int const tailleCase = 64;
int const larg = 10*tailleCase;


struct affiche{
	SDL_Window *window;
	SDL_Renderer *render;
};
typedef struct affiche affiche;
void freeAffiche(affiche *,int);
void init(affiche*,std::vector< std::vector<Case*> > &);
void freeCase(std::vector<std::vector <Case*> > &);
void affichePerso(affiche* aff,std::vector<Perso*> &perso);
