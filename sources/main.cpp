#include "Graphics/affichage.hpp"


int main(int argc,char* argv[]){
	affiche aff;
	std::string level = (std::string)argv[1];
	Niveau niveau("../ressources/Levels/level"+level,"../ressources/perso/perso"+level);
	std::vector <Perso*> perso = niveau.getPerso();
	std::vector< std::vector<Case*> > levelCase =niveau.getLevel();
	SDL_Event event;


	bool continuer = true;
	if(SDL_Init(SDL_INIT_VIDEO)==-1){
		std::cout << "erreur initialisation SDL "<<SDL_GetError()<<std::endl;
		return EXIT_FAILURE;
	}
	aff.window=SDL_CreateWindow("RacletteJAM",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,larg,larg,0);
	if(!aff.window){
		std::cout<<"erreur initialisation fenetre "<<SDL_GetError()<<std::endl;
		freeAffiche(&aff,1);
		SDL_Quit();
		return EXIT_FAILURE;
	}
	aff.render=SDL_CreateRenderer(aff.window,-1,SDL_RENDERER_ACCELERATED);
	if(!aff.render){
		std::cout<<"erreur initialisation render "<<SDL_GetError()<<std::endl;
		freeAffiche(&aff,0);
		SDL_Quit();
		return EXIT_FAILURE;
	}
	perso[0]->deplacement(0,0,levelCase,perso);
	init(&aff,levelCase);
	affichePerso(&aff,perso);
	SDL_RenderPresent(aff.render);
	while(continuer){
		while(SDL_PollEvent(&event)){
			switch(event.type){
				case SDL_QUIT:
					continuer = false;
					break;
				case SDL_KEYDOWN:

					switch(event.key.keysym.sym){

						case SDLK_UP:
							perso[0]->deplacement(0,-tailleCase,levelCase,perso);
							break;
						case SDLK_DOWN:
							perso[0]->deplacement(0,tailleCase,levelCase,perso);
							break;
						case SDLK_RIGHT:
							perso[0]->deplacement(tailleCase,0,levelCase,perso);
							break;
						case SDLK_LEFT:
							perso[0]->deplacement(-tailleCase,0,levelCase,perso);
							break;
					}
					init(&aff,levelCase);
					affichePerso(&aff,perso);
					continuer = !perso[0]->finJeu(levelCase);
					SDL_RenderPresent(aff.render);
			}
		}
	}
	freeCase(levelCase);
	freeAffiche(&aff,0);
	SDL_Quit();

	return EXIT_SUCCESS;
}
