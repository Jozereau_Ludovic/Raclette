#pragma once

#include <string>
#include <vector>
#include <stdio.h>
#include <math.h>

/*Couleurs*/
enum couleurs {RIEN = 0, ROUGE = 1, VERT = 2, BLEU = 3, JAUNE = 4, NOIR = 5}; //noir murs fixes

/*Types*/
enum types {SORTIE = 6, BOUTON = 7, CHANGEUR = 8, TP = 9};

class Case {
	private:
	
	int mPosX;
	int mPosY;
	bool mEtatBouton;
	std::vector<int> mCase; //{murs nord, ouest, sud, est, type, couleur type}
	std::vector<std::string> mImage; //{images murs nord, ouest, sud, est et type, couleur type}
	
	public:
	virtual ~Case(){}
	Case();
	Case(const Case &) = delete;
	Case(int, int, std::vector<int>, std::vector<std::string>);
	int getX() const;
	int getY() const;
	void setX(int x);
	void setY(int y);
	void setCase(int tagueule,int indice);
	void setImage(std::string image,int indice);
	std::vector<int> getCase();
	std::vector<std::string> getImage();
	void useButton(int, std::vector<std::vector<Case*> >);
	bool getEtatBouton();
	void setEtatBouton(bool );
	
};
