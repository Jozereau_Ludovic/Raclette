#include "Case.hpp"

Case::Case():mPosX(0), mPosY(0), mCase(6), mImage(5) {
}

Case::Case(int posX, int posY, std::vector<int> cases, std::vector<std::string> image) {
	mPosX = posX;
	mPosY = posY;
	mCase = cases;
	mImage = image;
}

int Case::getX() const {
	return mPosX;
}

int Case::getY() const {
	return mPosY;
}

std::vector<int> Case::getCase() {
	return mCase;
}

std::vector<std::string> Case::getImage() {
	return mImage;
}

void Case::useButton(int couleur, std::vector<std::vector<Case*> > caseCour) {
	unsigned int i, j, k;
	char cheminImage[255];
	
	
	for(i=0; i<caseCour.size(); ++i) {
		for(j=0; j<caseCour[i].size(); ++j) {
			for(k=0; k<4; ++k) {
				if (abs(caseCour[i][j]->mCase[k]) == couleur) {
					caseCour[i][j]->setCase(-caseCour[i][j]->mCase[k], k);
					sprintf(cheminImage, "../ressources/images/case/%d/%d.png", k, caseCour[i][j]->mCase[k]);
					caseCour[i][j]->setImage(cheminImage, k);
				}
			}
		}
	}
}
void Case::setX(int x){
	mPosX = x;
}
void Case::setY(int y){
	mPosY = y;
}

void Case::setCase(int tagueule,int indice){
	mCase[indice] = tagueule;
}
void Case::setImage(std::string image,int indice){
	mImage[indice]=image;
}

bool Case::getEtatBouton() {
	return mEtatBouton;
}
void Case::setEtatBouton(bool tonchien) {
	mEtatBouton = tonchien;
}
