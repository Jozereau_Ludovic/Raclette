#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "Case.hpp"
#include "Perso.hpp"


class Niveau {
	private:
	
	std::vector<std::vector<Case*> > mCase;
	std::vector<Perso*> mPerso;
	
	public:
	
	Niveau();
	Niveau(const Niveau &) = delete;
	Niveau(std::string, std::string);
	std::vector<std::vector<Case*> > getLevel() const ;
	std::vector<Perso*> getPerso() const;
};
